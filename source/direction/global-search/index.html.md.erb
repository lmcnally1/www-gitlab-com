---
layout: markdown_page
title: "Product Direction - Global Search"
description: "GitLab supports Advanced Search for GitLab.com and Self-Managed instances. This provides users with a faster and more complete search."
canonical_path: "/direction/global-search/"
---

- TOC
{:toc}

## Global Search

| Category | Description |
| ------ |  ------ |
| [Code Search](/direction/global-search/code-search/) | Search through all your code |
| Code Navigation | Navigate through all your code |

Global Search is managed by the [Global Search Group](https://about.gitlab.com/handbook/product/categories/#global-search-group) of the [Enablement stage](https://about.gitlab.com/direction/enablement/) as part of the [Enablement Section](https://about.gitlab.com/direction/enablement/). 

We encourage you to share feedback directly via [Twitter](https://twitter.com/BigCodeSearch) or [schedule a call](https://calendly.com/jmcguire-gitlab).

### Overview

Finding anything in GitLab should be straightforward.

Global Search is the core search feature for GitLab, as the one place to search across all projects, groups, and scopes.

There are two modes Global Search can operate in, depending on the instance configuration:

* [Basic Search](https://docs.gitlab.com/ee/user/search/#basic-search) is the default search mode for GitLab, requiring no additional configuration.
* [Advanced Search](https://docs.gitlab.com/ee/user/search/advanced_search.html) provides a richer search experience and is available at GitLab Premium self-managed, GitLab Premium SaaS, and higher tiers. In self-managed instances, it requires integration with [Elasticsearch](https://docs.gitlab.com/ee/integration/elasticsearch.html) or [OpenSearch](https://opensearch.org/docs/latest/).
    * **Faster searches:** Advanced Search, a purpose-built full-text search engine, can horizontally scale to provide search results in 1-2 seconds in most cases.
    * **Code Maintenance:** Finding all the code at once across an entire instance can save time spent maintaining code. Code Search is beneficial for organizations with a large number of developers.
    * **Promote innersourcing:** Your company may consist of many different developer teams, each of which has its group where the various projects are hosted. Some applications may be connected, so your developers must search throughout the GitLab instance and instantly find the code.

### Vision

In 3-years, Global Search makes all information easily findable across GitLab. Global Search includes the most frequently used content types, like issues, merge requests, or commits,  with search by Keyword functionality and only a few more advanced features like filtering and sorting.

First, we expect to improve the search experience's usability while improving the stability, scalability, and compatibility of the Elasticsearch integration.

Second, we plan to build upon the [Code Search](https://about.gitlab.com/direction/global-search/code-search/) capabilities while adding additional scopes to be searchable.

Third, we expect to make GitLab easier to use by connecting the existing content relationships to enable all [personas](https://about.gitlab.com/handbook/product/personas/).

#### Objectives

* Improve the user experience to make content easy to find by Q1/FY24
* Improve the stability, scalability, and compatibility of Elasticsearch by Q3/FY23
* Enable better Code Search capabilities as part of the [Code Search](https://about.gitlab.com/direction/global-search/code-search/) category by Q2/FY24
* Enable the ability to explore content across projects and groups and scopes FY25

### Desired Results

Global Search [Unique Monthly Users](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/enablement-section/#enablementglobal-search---paid-gmau---the-number-of-unique-paid-users-per-month) should grow at 10% MoM as we continue to see features and use cases enabled or improved. If Monthly users continue to grow, this will indicate that we are going in the right direction. If we see this decline consistently, it will signify that a correction in direction is needed. If the level is sustained over mo=ultiuple quarters, it could be seen that a new vision is needed, and we should consider changing the Metric from a focus on growth to a focus on quality, like result clickthrough rate. 

#### Maturity

The [maturity](https://about.gitlab.com/direction/maturity/) of Global Search Categories of Advanced Search and Code Search is "viable." We expect Global Search categories to be part of the [Product Direction Goal of having 50% of Categories in Lovable by the end of 2023](https://about.gitlab.com/company/strategy/#1-accelerate-market-maturity-around-the-devops-platform). GitLab's Advanced Search experience works for self-managed instances and across all Paid SaaS Experiences. UX Research and [SUS scores](https://about.gitlab.com/handbook/engineering/ux/performance-indicators/system-usability-scale/) have shown an immediate need to improve aspects of the UI and functionality of search capabilities.

Moving Advanced Search and Code Search to the Mturity stage "complete" will require these next steps:

1. Complete JTBD analysis
1. Complete UX scorecard for a baseline
1. Obtain scores that show an efficient level to promote the maturity to “complete.”

In congruence there are is Additional UX Research and deliverables from that research that should benefit the results of the maturity scorecard. 

#### Measuring Success

* **Performance Indicator Metrics:** We measure unique users per month, GMAU. Our goal is to grow GMAU 10% each month. [Internal Link](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/enablement-section/#enablementglobal-search---paid-gmau---the-number-of-unique-paid-users-per-month)
* **Error Budget Metrics:** Global Search Error Budgets are not complete. See [Proposal: create SLI's and SLO's for Global Search](https://gitlab.com/groups/gitlab-org/-/epics/7892)
* **Elasticsearch/Opensearch Version:** Because Advanced Search uses Elasticsearch/Opensearch, we track what versions are most commonly used by our customers. We use the version information to decide future deprecations and compatibility improvements. [Dashboard](https://app.periscopedata.com/app/gitlab/1018858/Version-of-Elasticsearch-Server-Used-by-SM-Instances)

### Blog posts about Global Search

We chronicled our journey of deploying Elasticsearch for GitLab.com through several blog posts.

* [2019-03-20 Lessons from our journey to enable global code search with Elasticsearch on GitLab.com](https://about.gitlab.com/blog/2019/03/20/enabling-global-search-elasticsearch-gitlab-com/)
* [2019-07-16 Update: The challenge of enabling Elasticsearch on GitLab.com](https://about.gitlab.com/releases/2019/07/16/elasticsearch-update/)
* [2020-04-28 Update: Elasticsearch lessons learnt for Advanced Search](https://about.gitlab.com/blog/2020/04/28/elasticsearch-update/)
* [2021-06-01 GitLab's data migration process for Advanced Search](https://about.gitlab.com/blog/2021/06/01/advanced-search-data-migrations/)

### What's Next:

We have added a live roadmap to track progress as we complete major epics.

[Global Search Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_desc&layout=WEEKS&label_name%5B%5D=group%3A%3Aglobal+search&label_name%5B%5D=Roadmap&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=ALL)

* [Improve Navbar UX to improve discoverability and usefulness of Global Search](https://gitlab.com/groups/gitlab-org/-/epics/7821)
* [Advanced Search should be compatible with the latest Elasticsearch and OpenSearch versions](https://gitlab.com/groups/gitlab-org/-/epics/7761)
    * [OpenSearch needs to work with GitLab Advanced Search](https://gitlab.com/groups/gitlab-org/-/epics/7777)
    * [Elasticsearch 8.x needs to work with GitLab Advanced Search](https://gitlab.com/groups/gitlab-org/-/epics/7778)
    * [Testing and QA for Elasticsearch and Opensearch Compatibility](https://gitlab.com/groups/gitlab-org/-/epics/7824)
     
### Competitive Landscape

Both GitHub and BitBucket provide a more comprehensive and complete search for users, particularly their ability to deeply search code and surface those results to users.

There is a great need to improve the ability to search across repositories for all competitors. Code search is commonly asked about by prospective customers and is included in the comparison matrix for our competitors. 

| Feature 	| GitLab 	| SourceGraph 	| GitHub 	| BitBucket 	| JIRA 	|
|-	|-	|-	|-	|-	|-	|
| Keyword Search <br>   Search with keywords not specifying a field 	| **YES** 	| **YES** 	| **YES** 	|  	|  No 	|
| Search Filters<br>   Filters can reduce results to more specific results 	| **YES** 	| **YES** 	| **YES** 	|  	| **YES** 	|
| NLP (Natural Language Processing)<br>   Search uses Linguistic structure to improve matching 	| **YES** 	| **YES** 	| **YES** 	|  	|  No 	|
| Language Code Filters <br>   Search detects Code Language and allows filtering to a specific Language 	|  No 	| **YES** 	| **YES** 	|  	|  	|
| Search Diffs <br>   Search returns results from previous versions 	|  No 	| **YES** 	| No 	|  	|  	|
| Commit message <br>   Search by text used for the Commit message 	| **YES** 	| **YES** 	|  **YES** 	|  	|  	|
| Saved searches <br>   The search with parameters can be saved and favorited to show later  	|  No 	| **YES** 	| No 	|  	|  	|
| Custom filter groupings <br>   Combine filters and save them as a filter grouping 	|  No 	| **YES** 	| No 	|  	|  	|
| Autocomplete <br>   Keywords are recommended while you are typing 	| **YES** 	| **YES** 	|  No 	|  	|  	|
| Rank statistics<br>   Results show a score variable between the results 	|  No 	| **YES** 	|  No 	|  	|  	|
| Multiple Branches <br>   Code search can show more than the main branch 	|  No 	| **YES** 	|  No 	|  	|  	|
| Compare results <br>   Result page allows you to compare a result to other results from other searches 	|  No 	|  No 	|  No 	|  	|  	|
| Comments <br>   Results are shown from comments 	| **YES** 	|  No 	|  **YES** 	|  	|  	|
| All Results <br>  Results are shown across all scopes or types in one integrated result page 	|  No 	| **YES** 	|  No 	|  	|  	|
| Keyboard Shortcuts <br>   Search is quickly accessible for key commands 	| **YES** 	| **YES** 	|  No  	|  	|  	|
| ID Quick Search <br>   Search Recognizes IDs and takes you directly to the item page 	| **YES** 	|  No 	|  No 	|  	|  	|
| Search by SHA<br>   Commit SHAs are recognized and you are directed to the sha instead of the results 	| **YES** 	|  No 	| No  	|  	|  	|
| File Search <br>   Files and directories are identified and searchable not just the reference of a file listed in code	| **YES** 	|  No 	| **YES** 	|  	|  	|
| SaaS <br>   Featurefull search is offered as part of the SaaS offering.  	| **YES** 	|  No 	| **YES** 	|  	|  	|
| APIs<br>   Users can query search using APIs directly 	| **YES** 	|  No 	| **YES** 	|  	|  	|
| Issues<br>   Issues are searchable 	| **YES** 	|  No 	| **YES** 	|  	|  	|
| Merge Request<br>   Merge requests are searchable (not just the code 	| **YES** 	|  No 	| **YES** 	|  	|  	|
| Wiki<br>   Wikis are searchable 	| **YES** 	|  No 	| **Yes** 	|  	|  	|
| Epics<br>   Epics are searchable 	| **YES** 	|  No 	| No	|  	|  	|
| Security Vulnerability<br>   Search results for vulnerabilites 	|  No 	|  No 	| No 	|  	|  	|
| Snipets<br>   Snipets are searchable 	|  No 	|  No 	|  No 	|  	|  	|
| Batch Changes<br>   Start batch changes from a result list 	|  No 	| **YES** 	| No 	|  	|  	|
| Search Across Groups<br>   Search across groups, Global Search 	| **YES** 	| **YES** 	| **YES** 	|  	|  	|
| COST <br>   Per seat license (avg) 	| **Included** 	| **$11** 	| **Included** 	|  	|  	|


<!-- ### Analyst Landscape -->
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues that will help us stay relevant from their perspective.-->



### Top user issue(s) <!-- This is probably the top popular issue from the category (i.e. the one with the most thumbs-up), but you may have a different item coming out of customer calls.-->



#### Basic Search <!--This [Issue List](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=Category%3AGlobal%20Search&label_name%5B%5D=customer&not%5Blabel_name%5D%5B%5D=Big%20Code) is used to check for customer intrest issues for Global Search.-->

- [Projects with a "was tag" should be easy to find in Basic Search ](https://gitlab.com/gitlab-org/gitlab/-/issues/14220)
- [Wiki search across all projects without requiring Advanced Search to be enabled.](https://gitlab.com/gitlab-org/gitlab/-/issues/15835)

#### Advanced Search <!--This [Issue List](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=Category%3AGlobal%20Search&label_name%5B%5D=customer&label_name%5B%5D=advanced%20search&not%5Blabel_name%5D%5B%5D=Big%20Code) is used to check for customer intrest issues for Advanced Search.-->

- [Use elasticsearch for filtered searches of issues and merge requests if available](https://gitlab.com/gitlab-org/gitlab/-/issues/12082)
- [Index Microsoft Word files in ElasticSearch](https://gitlab.com/gitlab-org/gitlab/-/issues/13681)

### Top internal customer issue(s) <!--This [Issue List](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=Category%3AGlobal%20Search&not%5Blabel_name%5D%5B%5D=Big%20Code) is used to check for Internal intrest issues for Global Search.-->
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->
- [Advanced Search: Create new rake task with integration details](https://gitlab.com/gitlab-org/gitlab/-/issues/340437)
- [Expose search API in GraphQL](https://gitlab.com/gitlab-org/gitlab/-/issues/27417)

### Top Strategy Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Global Search Category Maturity](https://gitlab.com/groups/gitlab-com/-/epics/1271)
- [Elasticsearch Indexer - Evaluate FIPS compliance](https://gitlab.com/gitlab-org/gitlab/-/issues/296018)
