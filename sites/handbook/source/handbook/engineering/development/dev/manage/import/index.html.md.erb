---
layout: handbook-page-toc
title: "Import Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Manage:Import
{: #welcome}


### How we work

* In accordance with our [GitLab values](/handbook/values/).
* Transparently: nearly everything is public, we record/livestream meetings whenever possible.
* We get a chance to work on the things we want to work on.
* Everyone can contribute; no silos.
  * The goal is to have product give engineering and design the opportunity to be involved with direction and issue definition from the very beginning.
* We do an optional, asynchronous daily stand-up in our respective group stand-up channels:
  * Manage:Import [#g_manage_import_daily](https://gitlab.slack.com/archives/C01099NRZ7B)

#### Prioritization
<%= partial "handbook/engineering/development/dev/manage/prioritization.erb", locals: { filter_value: 'import' } %>

#### Organizing the work

We generally follow the [Product Development Flow](/handbook/product-development-flow/#workflow-summary):
1. `workflow::problem validation` - needs clarity on the problem to solve
1. `workflow::design` - needs a clear proposal (and mockups for any visual aspects)
1. `workflow::solution validation` - needs refinement and acceptance from engineering
1. `workflow::planning breakdown` - needs a Weight estimate
1. `workflow::scheduling` - needs a milestone assignment
1. `workflow::ready for development`
1. `workflow::in dev`
1. `workflow::in review`
1. `workflow::verification` - code is in production and pending verification by the DRI engineer

Generally speaking, issues are in one of two states:
* Discovery/refinement: we're still answering questions that prevent us from starting development,
* Implementation: an issue is waiting for an engineer to work on it, or is actively being built.

Basecamp thinks about these stages in relation to the [climb and descent of a hill](https://www.feltpresence.com/hills.html).

While individual groups are free to use as many stages in the [Product Development Flow](/handbook/product-development-flow/#workflow-summary) workflow as they find useful, we should be somewhat prescriptive on how issues transition from discovery/refinement to implementation.

##### Backlog management

Backlog management is very challenging, but we try to do so with the use of labels and milestones.

###### Refinement

**The end goal is defined,** where all direct stakeholders says “yes, this is ready for development”. Some issues get there quickly, some require a few passes back and forth to figure out.

The goal is for engineers to have buy-in and feel connected to the roadmap. By having engineering included earlier on, the process can be much more natural and smooth.


We have a weekly refinement process which consists of the following steps:

* By Thursday every week Product Manager and Engineering Manager will add issues to the refinement board and label the 3 most important issues `ready for next refinement`.
* In the following week, engineers, PM and UX will refine these 3 issues in the `ready for next refinement` column over a sync call (Tuesday, 08:30am CET).
* Before the sync call, engineers and UX should familiarize themselves with the `ready for next refinement` issues and bring up any questions in the issue.
    * Any outcome of the sync call discussion (problems, solution proposal, etc.) needs to be documented in the issue.
    * Once an issue is refined and a weight has been added the `workflow::ready for development` label (along with the `Next Up` label which should already be there) should be added. Also, the `ready for next refinement` label should be removed afterwards. This will automatically add the issue to the build board.
    * Engineers need to make sure that issues will be added at the bottom of the `workflow::ready for development` column (otherwise priorities don't match anymore).
* We will be reviewing the process in the monthly retrospective issues over the next 2-3 milestones (until 15.4) to make further adjustments if needed.

###### Next Up

* To identify issues that need refinement, use the "Next Up" label.
  * The purpose of the "Next Up" label is to identify issues that are currently in _any_ workflow stage before `workflow::ready for development`. By using this "Next Up" label in addition to workflow labels, we're able to see exactly what is being refined, e.g., problem, design, solution. This helps identify which issues are closer to being ready to schedule.
* Issues shouldn't receive a milestone for a specific release (e.g. 13.0) until they've received a 👍 from both Product and Engineering. This also means the issue should not be labeled as `workflow::ready for development`.
  * Product approval is represented by an issue moving into `workflow::planning breakdown`.
  * Engineering approval is represented by an issue weight measuring its complexity.

##### Breaking down or promoting issues

Depending on the complexity of an issue, it may be necessary to break down or promote issues. A couple sample scenarios may be:

- We need to do discovery on the design, before we do anything else. A "Discovery:" issue may work best here as it helps to contain the design thinking and discussion there, with the end result being transferred over to a "Implementation:" issue. These prefixes also help to organize what type of issue they are, in the case they are linked to parent issues or epics.
- The scope of work is larger than anticipated, and needs to be broken down further, e.g., it currently has a weight higher than 5. It may suit you to then promote said issue to an epic, to break it down into smaller issues to list out the different iterations or phases of work that need to happen to deliver the overall feature that was originally proposed.
- The scope of work is clear, but a bit unwieldy for one issue. It may make sense to keep the given issue as is, to keep the conversation and activity visible to everyone, but create separate child design, backend, or frontend issues to track the more nuanced progress of a given issue.

If none of the above applies, then the issue is probably fine as-is! It's likely then that the weight of this issue is quite low, e.g., 1-2.

##### Managing discussions, information, decisions, and action items in an issue.

As part of [breaking down or promoting issues](#breaking-down-or-promoting-issues), you may find that there are a significant number of threads and comments in a given issue.

It's very important that we make sure any proposal details, pending action items, and decisions are easily visible to any stakeholder coming into an issue. Therefore, it's paramount that the issue description is kept up-to-date, or otherwise broken down or promoted as per the above section.

#### Estimation

<%= partial("handbook/engineering/development/dev/manage/estimation.erb") %>

#### Planning

<%= partial("handbook/engineering/development/dev/manage/planning.erb") %>

#### Working with Security

The Import group has an existing [threat model](https://gitlab.com/gitlab-com/gl-security/appsec/threat-models/-/blob/master/gitlab-org/gitlab/GitLab%20Migration.md) to assist in identifying issues that may have security implications, but there are other considerations. 

An [Application Security Review](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/appsec-reviews.html) should be requested when the issue or MR might have security implications. These include, but aren't limited to, issues or MRs which:
- falls under the threat model
- handles binary files (downloading, decompressing, extracting, moving, deleting)
- modifies or uses file manipulation services
- uses methods from Import/Export `CommandLineUtil`


#### During a release

<%= partial("handbook/engineering/development/dev/manage/release.erb") %>

#### Proof-of-concept MRs

<%= partial("handbook/engineering/development/dev/manage/pocs.erb") %>

### Working on unscheduled issues

<%= partial("handbook/engineering/development/dev/manage/unscheduled_issues.erb") %>

### Additional considerations

<%= partial("handbook/engineering/development/dev/manage/import_processes.erb") %>


## Meetings

Although we have a bias for asynchronous communication, synchronous meetings are necessary and should adhere to our [communication guidelines](/handbook/communication/#video-calls). Some regular meetings that take place in Manage are:

| Frequency | Meeting                              | DRI         | Possible topics                                                                                        |
|-----------|--------------------------------------|-------------|--------------------------------------------------------------------------------------------------------|
| Weekly    | Group-level meeting                  | Backend Engineering Managers | Ensure current release is on track by walking the board, unblock specific issues                       |
| Monthly   | Planning meetings                    | Product Managers         | See [Planning](/handbook/engineering/development/dev/manage/#planning) section |

For one-off, topic specific meetings, please always consider recording these calls and sharing them (or taking notes in a [publicly available document](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit)).

Agenda documents and recordings can be placed in the [shared Google drive](https://drive.google.com/drive/u/0/folders/0ALpc3GhrDkKwUk9PVA) (internal only) as a single source of truth.

Meetings that are not 1:1s or covering confidential topics should be added to the Manage Shared calendar.

All meetings should have an agenda prepared at least 12 hours in advance. If this is not the case, you are not obligated to attend the meeting. Consider meetings canceled if they do not have an agenda by the start time of the meeting.


## Group Members

The following people are permanent members of the group:

<%= stable_counterparts(role_regexp: /Manage.+Import/) %>

## Dashboards 

- [Backend overview](https://app.periscopedata.com/app/gitlab/698723/Manage::Import-Backend-Overview)
- [North star metrics](https://app.periscopedata.com/app/gitlab/661967/Manage:Import-Dashboard)


## Links and resources
{: #links}

<%= partial("handbook/engineering/development/dev/manage/shared_links.erb") %>
* [Milestone retrospectives](https://gitlab.com/gl-retrospectives/manage-stage/import/-/issues)
* Our Slack channels
  * Manage:Import [#g_manage_import](https://gitlab.slack.com/messages/CLX7WMSKW)
  * Daily standups [#g_manage_import_daily](https://gitlab.slack.com/archives/C01099NRZ7B)
* Issue boards
  * Import [build board](https://gitlab.com/groups/gitlab-org/-/boards/4172737) and [refinement board](https://gitlab.com/groups/gitlab-org/-/boards/4175527)
* Onboarding videos (GitLab Unfiltered Youtube)
  * [Manage:Import Onboarding - GitLab Migration](https://www.youtube.com/watch?v=vVQ6Ex9fSl8)
  * [Manage:Import Onboarding - Introduction to GitHub Importer](https://www.youtube.com/watch?v=TxHopzXop5s)
  * [Manage:Import Onboarding - File based GitLab Import/Export](https://www.youtube.com/watch?v=A4kdpnbhmcw)
  * [Remote S3 Import Example](https://www.youtube.com/watch?v=I85SXNmiS_k)
