---
layout: handbook-page-toc
title: "Materials Legal Review Process"
description: "Follow this process to obtain legal review of materials for internal and external use"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Using this process
#### Scope
This process applies to:

* Internal and external materials requiring approval as determined by the [SAFE Framework Flowchart](/handbook/legal/safe-framework/#safe-flowchart);
* External materials containing any third-party intellectual property including customer and partner logos; 
* Materials prepared for large-scale internal and external events, like SKO, Partner SKO, Commit, and Contribute;
* External materials which quote, or are written in the name of, a member of the [GitLab Executive Group](/company/team/e-group/) (eGroup), including such materials prepared for speaking engagements at non-GitLab events, interviews, press releases, and guest content bylined by a member of the eGroup; and
* Internal materials which quote, or are written in the name of, a member of eGroup, including GitLab Assembly materials, GitLab newsletters, manager newsletters, company wide Slack messages.

The Legal & Corporate Affairs team will triage materials submitted for review, and escalate to other team members up to and including the Chief Legal Officer, as appropriate.

This process does **not** apply to:

* Agreements of any kind.
* [AMAs](/handbook/communication/ask-me-anything/), [Key Review](/handbook/key-review/) meetings, and [Group Conversations](/handbook/group-conversations/) streamed or published [publicly](/handbook/marketing/marketing-operations/youtube/#visibility) to GitLab Unfiltered. However, these recordings should not be streamed or published if they contain confidential information;
* External or internal handbook updates;
* GitLab issues or merge requests;
* Internal and external communications prepared in relation to _a contentious issue that undermines a critical attribute of our company’s brand and challenges its reputation or impacts, or has the potential to impact, the safety and wellbeing of our team-members_. Communications for such situations should be prepared in accordance with the [GitLab Incident Communications Plan](/handbook/marketing/corporate-marketing/incident-communications-plan/); and
* Blog posts. A separate workstream is running to develop a SAFE review process for the review of blog posts that does not involve the use of public MRs; this process will be added in a future iteration.

The Legal & Corporate Affairs Team will not approve materials in Slack because [Slack should not be used for obtaining approval](/handbook/communication/#slack); follow this process instead. However, if you’re unsure whether this process applies to the materials you are preparing, reach out to [#legal](https://app.slack.com/client/T02592416/C78E74A6L) to confirm. 


#### External vs internal use

**External use** means materials made available to any company or individual who is not a GitLab team member, including media, marketing audiences, prospects, customers, partners, vendors, community members, and conference attendees. Materials published with [public visibility](/handbook/marketing/marketing-operations/youtube/#visibility) on the GitLab Unfiltered YouTube channel, or on any GitLab social media channel, are external.

**Internal use** means materials made available solely to GitLab team members.

Sometimes, legal review entails two separate reviews:

* Materials for external use are reviewed (i) against the [SAFE Framework](/handbook/legal/safe-framework/), and (ii) for compliance with third-party intellectual property (IP) rights; and
* Materials for internal use are reviewed solely against the [SAFE Framework](/handbook/legal/safe-framework/); an IP review is not required unless it is contemplated that such materials will be used externally.

When creating materials:

* for external use, refer to the [SAFE Framework](/handbook/legal/safe-framework/) and the [Guidelines for Use of Third-party IP in External Materials](/handbook/legal/ip-public-materials-guidelines/).
* for internal use, refer to the [SAFE Framework](/handbook/legal/safe-framework/).


#### Turnaround time

The Legal & Corporate Affairs Team aims to complete single material reviews within two business days of submission. The turnaround time for multiple materials reviews will depend on the volume of material submitted for review. For events involving the production of a large volume of material, like SKO, Partner SKO, Commit, and Contribute, allow at least two weeks for review.


#### Requesting review of videos not yet produced

If the review request relates to a video not yet produced, review and approval is required in respect of **both** (i) the slide deck, storyboard and script (as appropriate, and in final form) prior to recording, **and** (ii) the final cut. Request review at each stage as set out in this process.


#### Two-track process

This process is divided into two tracks - follow the track which applies to your situation:
- To obtain legal review of a single piece of material, follow [Track 1: Single material legal review process](./#track-1-single-material-legal-review-process).
- To obtain legal review of multiple pieces of material, follow [Track 2: Multiple materials legal review process](./#track-2-multiple-materials-legal-review-process).

----
### Track 1 Single material legal review process

Follow this process to obtain legal review of a **single piece of material**. A slide deck, script, and video for one presentation are considered a single piece of material.

1. If a **confidential** working issue for the material already exists, make a comment in that issue using the following template. The template is written in markdown - paste it into a new comment and fill in the blanks.

```
### Materials to be reviewed
<!-- Link (for Google Docs) or upload (for other file types) the material for review here. -->    

### Is this material for internal or external use?
<!-- Delete as appropriate, and refer to the definitions of `external use` and `internal use` in the Materials Legal Review Process. If there are plans to use the material, or any part of it, externally in the future, chose `external`. -->
- external @hloeffert @LeeFalc
- internal @hloeffert

### Will the materials be made available on GitLab Unfiltered, Edcast, or anywhere else?
<!-- Delete as appropriate to state whether some or all of the materials being submitted for review will be made available anywhere. If they will, give details of the visibility the materials will have. -->
- yes <!-- if yes, give details -->
- no

### Due date for review
<!-- State the due date for review, and indicate this as the due date of the issue below, noting that the Legal & Corporate Affairs Team requires at least two business days to complete a review. -->
```

2. If no working issue already exists, complete the [single material legal review issue template](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=single-material-legal-review), following the instructions in the template.

3. Note that:
   * To obtain the review appropriate to your proposed use, you must tag the correct Legal & Corporate Affairs Team members in the issue description ([`@hloeffert`](gitlab.com/hloeffert) for internal materials, and [`@hloeffert`](gitlab.com/hloeffert) and [`@LeeFalc`](gitlab.com/LeeFalc) for external materials). IP review is not required for materials created for internal use, only SAFE review is required. Internal use and external use are [defined here](#external-vs-internal-use).
   * The Legal & Corporate Affairs Team aims to review materials **within two business days of submission**; clearly state the due date when creating the issue as the template requires.

4. Put a link in the material being reviewed to the legal review issue or comment you created:
   * Google Docs: add the link at the top of the first page of the doc.
   * Google Slides: add the link to the first slide.
   * PDFs: no link required.

5. If the review request relates to a video not yet produced, review and approval is required in respect of **both** (i) the slide deck, storyboard, and script (as appropriate, and in final form) before recording, **and** (ii) the final cut. Request review at each stage as set out in this process.

6. The Legal & Corporate Affairs Team will review the material, making comments and requests for changes in the document, or an issue comment thread, and provide context for any requested changes in accordance with the [say why, not just what](/handbook/values/#say-why-not-just-what) sub-value.

7. Once legal review is complete and the material approved, a [Legal & Corporate Affairs Team member](https://about.gitlab.com/company/team/?department=legal-corporate-affairs) will tag the issue creator and comment `Legal review complete: material approved`. The material is approved only once `Legal review complete: material approved `has been commented; comments like `SAFE review complete` and `IP review complete` **do not indicate approval**.

8. If **any** changes are made to the material after legal approval, another legal review is required. Tag the appropriate reviewers (([`@hloeffert`](gitlab.com/hloeffert) for internal materials, and [`@hloeffert`](gitlab.com/hloeffert) and [`@LeeFalc`](gitlab.com/LeeFalc) for external materials)) in the issue, or issue comment, created in step 1, requesting review of the amended material. As repeat reviews are inefficient, ensure materials are finalized before submitting for legal approval.

9. Once legal review of the amended material is complete and the material approved, a [Legal & Corporate Affairs Team member](https://about.gitlab.com/company/team/?department=legal-corporate-affairs) will once again tag the issue creator and comment `Legal review complete: material approved`.

----
### Track 2 Multiple materials legal review process

Follow this process to obtain legal review of **multiple pieces of material** with a related purpose, like several slide decks being prepared for one event.

1. Complete the [multiple materials review issue template](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=multiple-materials-legal-review), following the instructions in the template. Note that:
   * IP review is not required for materials created for [internal use](#external-vs-internal-use), only SAFE review is required;
   * For a given related purpose (like an event), only one multiple materials review issue needs to be created;
   * For multiple materials legal review requests involving five or fewer pieces of material, legal aims to complete the review within 5 business days of submission; and
   * For multiple materials legal review requests involving more than five pieces of material, the DRI must, no less than 5 business days before the materials will be ready for review, (i) notify the Legal & Corporate Affairs Team in [#legal](https://app.slack.com/client/T02592416/C78E74A6L) of the upcoming event, and (ii) arrange a sync with the Team to agree a timeline for the completion of the review.

2. As each of piece of material is ready for review, make a comment in the issue which:
   * Tags the appropriate reviewers - [`@hloeffert`](gitlab.com/hloeffert) for internal materials, and [`@hloeffert`](gitlab.com/hloeffert) and [`@LeeFalc`](gitlab.com/LeeFalc) for external materials - requesting review;
   * Links (for Google Docs) or uploads (for other files) the material to the comment for review; and
   * States the due date for the review.

3. Put a link to the comment thread you created in the material being reviewed as follows:
   * Google Docs: add the link at the top of the doc.
   * Google Slides: add the link to the first slide.
   * PDFs: no link required.

4. The Legal & Corporate Affairs team will review the material, making comments and requests for changes in the document, or the applicable comment thread in the issue, and provide context for any requested changes in accordance with the [say why, not just what](/handbook/values/#say-why-not-just-what) sub-value. **Ensure that all discussion relating to a given piece of material takes place in [replies to the relevant comment thread](https://docs.gitlab.com/ee/user/discussions/#create-a-thread-by-replying-to-a-standard-comment)**; do not create a new comment thread each time you comment.

5. Once legal review is complete and the material approved, a [Legal & Corporate Affairs Team member](https://about.gitlab.com/company/team/?department=legal-corporate-affairs) will tag the creator of the comment thread and comment `Legal review complete: material approved` in the relevant comment thread. The material is approved only once `Legal review complete: material approved `has been commented; comments like `SAFE review complete` and `IP review complete` **do not indicate approval**.

6. If **any** changes are made to the material after legal approval, another legal review is required. Tag the appropriate reviewers ([`@hloeffert`](gitlab.com/hloeffert) for internal materials, and [`@hloeffert`](gitlab.com/hloeffert) and [`@LeeFalc`](gitlab.com/LeeFalc) for external materials) in the issue comment created in step 2, requesting review of the amended material. As repeat reviews are inefficient, ensure materials are finalized before submitting for legal approval.

7. Once legal review of the amended material is complete and the material approved, a [Legal & Corporate Affairs Team member](https://about.gitlab.com/company/team/?department=legal-corporate-affairs) will once again tag the issue creator and comment `Legal review complete: material approved` in the relevant comment thread.

8. If the review request relates to a video not yet produced, review and approval is required in respect of **both** the slide deck, storyboard and script (as appropriate) prior to recording **and** the final cut. Request review at each stage as set out in this process.
